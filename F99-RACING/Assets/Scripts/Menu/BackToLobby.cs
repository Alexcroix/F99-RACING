using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BackToLobby : MonoBehaviourPunCallbacks
{
    public void OnButtonClick()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("lobby");
    }
}
