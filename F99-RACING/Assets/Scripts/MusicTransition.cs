using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicTransition : MonoBehaviour
{
    private static MusicTransition instance;
    public AudioSource audioSource;
    void Awake()
    {
        if (instance is null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Update()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene == SceneManager.GetSceneByName("Circuit1") ||
            currentScene == SceneManager.GetSceneByName("Circuit2") ||
            currentScene == SceneManager.GetSceneByName("Circuit_Ach")||
            currentScene == SceneManager.GetSceneByName("Circuit1IA")||
            currentScene == SceneManager.GetSceneByName("Circuit2IA")
            )
            
        {
            audioSource.Stop();
        }
    }
}
