using System.Collections;
using System.Collections.Generic;
using Photon.Pun.Demo.Cockpit;
using UnityEngine;

public class CarEngine : MonoBehaviour
{
    public Collider car;
    public Transform path;
    public float maxSteerAngle = 45f;
    public WheelCollider wheelFL;
    public WheelCollider wheelFR;
    private List<Transform> nodes;
    private int currectNode = 0;

    public float max_speed = 0.05f;
    public bool isAccelerating = false;

    void Start()
    {
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
        //StartCoroutine(acceleration());
    }


    private void Update()
    {
        ApplySteer();
        if (!isAccelerating)
            Drive();
    }

    private void NextNode()
    {
        //StartCoroutine(acceleration());
        currectNode++;
        if (currectNode >= nodes.Count)
        {
            currectNode = 0;
        }
    }

    private void ApplySteer()
    {
        
        Transform carTransform = car.transform;
        carTransform.LookAt(nodes[currectNode]);
        Transform Ltransform = wheelFL.transform;
        Ltransform.LookAt(nodes[currectNode]);
        Transform Ltransforms = wheelFR.transform;
        Ltransforms.LookAt(nodes[currectNode]);
        if (Vector3.Distance (nodes[currectNode].position, wheelFL.transform.position) <= 5f)
        {
            StopAllCoroutines();
            NextNode();
        }
    }

    IEnumerator acceleration()
    {
        isAccelerating = true;
        Vector3 dir =  car.transform.position - nodes[currectNode].position;
        float current_speed = 1;
        max_speed = Mathf.Clamp(Mathf.Abs(dir.x), 5f, 5f);
        float step = (max_speed-current_speed) / 150;
        while (current_speed < max_speed)
        {
            Vector3 dir2 =  car.transform.position - nodes[currectNode].position;
            float temp = Mathf.Clamp(Mathf.Abs(dir2.x*100), 0.1f, 1f);
            car.transform.position = Vector3.Lerp(car.transform.position, nodes[currectNode].position, 0.2f *Time.deltaTime * current_speed * temp);
            current_speed += step;
            yield return new WaitForSeconds(0.01f);
        }

        isAccelerating = false;
    }

    public void Drive()
    {
        Vector3 dir =  car.transform.position - nodes[currectNode].position;
        float temp = Mathf.Clamp(Mathf.Abs(dir.x/100f), 0.1f, 2f);
        car.transform.position = Vector3.Lerp(car.transform.position, nodes[currectNode].position, 0.3f * Time.deltaTime * max_speed / temp);
    }
}