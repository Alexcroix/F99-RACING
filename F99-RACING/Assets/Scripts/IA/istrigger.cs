using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;


public class istrigger : MonoBehaviourPun
{
    public GameObject voiture;
    public GameObject IA;
    public GameObject IA2;
    public GameObject roue1;
    public GameObject roue2;
    public GameObject roue3;
    public GameObject roue4;
    void Start()
    {
        GameObject car = GameObject.FindWithTag("IA");
        IA = car.transform.Find("Body").gameObject;

        if (IA != null)
        {
            Physics.IgnoreCollision(voiture.GetComponent<Collider>(), IA.GetComponent<Collider>());
            Physics.IgnoreCollision(roue1.GetComponent<Collider>(), IA.GetComponent<Collider>());
            Physics.IgnoreCollision(roue2.GetComponent<Collider>(), IA.GetComponent<Collider>());
            Physics.IgnoreCollision(roue3.GetComponent<Collider>(), IA.GetComponent<Collider>());
            Physics.IgnoreCollision(roue4.GetComponent<Collider>(), IA.GetComponent<Collider>());
        }
        
        
        GameObject car2 = GameObject.FindWithTag("IA2");
        IA2 = car2.transform.Find("Body").gameObject;

        if (IA2 != null)
        {
            Physics.IgnoreCollision(voiture.GetComponent<Collider>(), IA2.GetComponent<Collider>());
            Physics.IgnoreCollision(roue1.GetComponent<Collider>(), IA2.GetComponent<Collider>());
            Physics.IgnoreCollision(roue2.GetComponent<Collider>(), IA2.GetComponent<Collider>());
            Physics.IgnoreCollision(roue3.GetComponent<Collider>(), IA2.GetComponent<Collider>());
            Physics.IgnoreCollision(roue4.GetComponent<Collider>(), IA2.GetComponent<Collider>());
        }
        
    }
    
    [PunRPC]
    public void LOOSE()
    {
        Debug.Log("dans le rpc");
        SceneManager.LoadScene("LOOSE");
    }
    
}
